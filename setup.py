from os.path import dirname, join
from setuptools import find_packages, setup


setup(
    name='flake8-legalsense',
    license='MIT',
    version="0.0.1",
    description='Plugin to enforce internal coding standards in Legalsense project',
    long_description_content_type='text/markdown',
    author='Kamil Niski',
    author_email='k.niski@sunscrapers.com',
    packages=find_packages(exclude=('tests', 'tests.*')),
    include_package_data=True,
    entry_points={
        'flake8.extension': [
            'LS0 = flake8_legalsense:LegalsenseStyleChecker',
        ],
    },
    install_requires=['flake8'],
    tests_require=['pytest'],
    classifiers=[
        'Framework :: Flake8',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Topic :: Software Development :: Quality Assurance',
    ],
)
