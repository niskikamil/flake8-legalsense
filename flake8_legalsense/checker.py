import ast
import astroid
from flake8_legalsense.checkers.model_form import ModelFormChecker, ModelFormFactoryChecker
from flake8_legalsense.checkers.form import FormChecker


def add_fields_attr(node):
    node._fields = node._astroid_fields
    return node

for node_class in astroid.nodes.ALL_NODE_CLASSES:
    astroid.MANAGER.register_transform(node_class, add_fields_attr)


def iter_fields(node):
    """
    Yield a tuple of ``(fieldname, value)`` for each field in ``node._fields``
    that is present on *node*.
    """
    for field in node._astroid_fields:
        try:
            yield field, getattr(node, field)
        except AttributeError:
            pass


class LegalsenseStyleFinder(ast.NodeVisitor):
    """
    Visit the node, and return issues.
    """
    checkers = {
       'Call': [
           ModelFormFactoryChecker(),
#            ModelFieldChecker(),
#            URLChecker(),
#            RenderChecker(),
       ],
        'ClassDef': [
            ModelFormChecker(),
            FormChecker(),
        ]
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.issues = []
    
    def visit(self, node):
        """Visit a node."""
        method = 'visit_' + node.__class__.__name__
        visitor = getattr(self, method, self.generic_visit)
        return visitor(node)

    def generic_visit(self, node):
        """Called if no explicit visitor function exists for a node."""
        for field, value in iter_fields(node):
            if isinstance(value, list):
                for item in value:
                    if isinstance(item, astroid.node_classes.NodeNG):
                        self.visit(item)
            elif isinstance(value, astroid.node_classes.NodeNG):
                self.visit(value)

    def capture_issues_visitor(self, visitor, node):
        for checker in self.checkers[visitor]:
            issues = checker.run(node)
            if issues:
                self.issues.extend(issues)
        self.generic_visit(node)

    def visit_Call(self, node):
        self.capture_issues_visitor('Call', node)

    def visit_ClassDef(self, node):
        self.capture_issues_visitor('ClassDef', node)


class LegalsenseStyleChecker(object):
    """
    Check common Legalsense Style errors
    """
    options = None
    name = 'flake8-legalsense'
    version = "0.0.1"

    def __init__(self, tree, filename):
        self.tree = tree
        self.filename = filename

    def run(self):
        parser = LegalsenseStyleFinder()
        tree = astroid.builder.AstroidBuilder().file_build(self.filename)
        parser.visit(tree)
        # parser.visit(self.tree)

        for issue in parser.issues:
            yield (issue.lineno, issue.col, issue.message, LegalsenseStyleChecker)
