import astroid


class Checker(object):
    """
    Abstract class for Checkers.
    """

    def get_call_name(self, node):
        """
        Return call name for the given node.
        """
        if isinstance(node.func, astroid.Attribute):
            return node.func.attrname
        elif isinstance(node.func, astroid.Name):
            return node.func.name

    def run(self, node):
        """
        Method that runs the checks and returns the issues.
        """
        return NotImplementedError  # pragma: no cover
