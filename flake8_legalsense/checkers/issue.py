class Issue(object):
    """
    Abstract class for issues.
    """
    code = ''
    description = ''

    def __init__(self, lineno, col, parameters=None):
        self.parameters = {} if parameters is None else parameters
        self.col = col
        self.lineno = lineno

    @property
    def message(self):
        """
        Return issue message.
        """
        message = self.description.format(**self.parameters)
        return '{code} {message}'.format(code=self.code, message=message)


class LS01(Issue):
    code = 'LS01'
    description = 'In Legalsense all ModelForm Meta must have localized_fields'


class LS02(Issue):
    code = 'LS02'
    description = 'In Legalsense all Form has to have localized=True'


class LS03(Issue):
    code = 'LS03'
    description = 'In Legalsense all modelform_factory must have localized_fields'