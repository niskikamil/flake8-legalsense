from functools import partial
import operator

import astroid

from . import issue
from .base_model_checker import BaseChecker
from .checker import Checker


class ModelFormChecker(BaseChecker):
    name_lookup = 'ModelForm'

    def checker_applies(self, node: astroid.ClassDef):
        return any(
            self.is_name_lookup(base) or self.is_name_lookup_attribute(base) for base in node.ancestors()
        )

    def run(self, node: astroid.ClassDef):
        """
        Detects absence of localized_fields in form Meta
        """

        if not self.checker_applies(node):
            return

        issues = []
        for body in node.body:
            if not isinstance(body, astroid.ClassDef):
                continue
            if body.name != "Meta":
                continue
            localized_fields_found = False
            for element in body.body:
                if not isinstance(element, astroid.Assign):
                    continue
                localized_fields_found = any(target.name == 'localized_fields' for target in element.targets)
                if localized_fields_found:
                    break
            if not localized_fields_found:
                issues.append(
                    issue.LS01(
                        lineno=node.lineno,
                        col=node.col_offset,
                    )
                )
        return issues


class ModelFormFactoryChecker(Checker):

    def checker_applies(self, node: astroid.Call):
        return self.get_call_name(node) in {"modelform_factory", "modelformset_factory", "inlineformset_factory"}

    @staticmethod
    def should_add_issue(node: astroid.Call):
        if node.keywords is not None:
            call_keywords = set(keyword.arg for keyword in node.keywords)
            return "form" not in call_keywords and "localized_fields" not in call_keywords
        return True

    def run(self, node: astroid.Call):
        """
        Detects absence of localized_fields in form/formset factories

        The issue is raised only when one of the factories has no form kwarg overridden.
        We assume if form is passed to the factory other checkers already made sure it
        conforms to localization rules.
        Thus only case when form and localized_fields kwargs are absent is triggered.
        """

        if not self.checker_applies(node):
            return

        issues = []
        if self.should_add_issue(node):
            issues.append(
                issue.LS03(
                    lineno=node.lineno,
                    col=node.col_offset,
                )
            )
        return issues