import astroid
from django.forms import fields
from .base_model_checker import BaseChecker
from . import issue

class FormChecker(BaseChecker):
    name_lookup = 'Form'

    def checker_applies(self, node):
        return any(
            self.is_name_lookup(base) or self.is_name_lookup_attribute(base) for base in node.ancestors()
        )

    def run(self, node):
        """
        Detects absence of localized_fields in form Meta
        """

        if not self.checker_applies(node):
            return

        issues = []
        for element in node.body:
            if isinstance(element, astroid.Assign) and isinstance(element.value, astroid.Call):
                field_node = next(element.value.func.infer())
                if any(getattr(base, "name", getattr(base, "attrname", "")) == "Field" for base in field_node.ancestors()):
                    if element.value.keywords is None or not any('localize' == keyword.arg for keyword in element.value.keywords):
                        issues.append(
                            issue.LS02(
                                lineno=element.lineno,
                                col=element.col_offset,
                            )
                        )
        return issues
