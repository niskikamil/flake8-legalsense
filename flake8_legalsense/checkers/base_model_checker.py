import ast

import astroid

from .checker import Checker


class BaseChecker(Checker):
    """
    Base class for checkers that must lookup for Model like nodes.
    """

    name_lookup = None

    def is_name_lookup(self, base, lookup=None):
        """
        Return True if class is defined as the respective model name lookup declaration
        """
        lookup = lookup if lookup is not None else self.name_lookup
        return (
            isinstance(base, astroid.ClassDef) and
            getattr(base, "name", getattr(base, "attrname", "")) == lookup
        )

    def is_name_lookup_attribute(self, base, lookup=None):
        """
        Return True if class is defined as the respective model name lookup declaration
        """
        lookup = lookup if lookup is not None else self.name_lookup

        return (
            isinstance(base, astroid.Attribute) and
            isinstance(base.value, astroid.ClassDef) and
            base.value.id in {'forms', 'd_forms'} and base.attr == lookup
        )
